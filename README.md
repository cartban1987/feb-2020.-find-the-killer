# Feb 2020. Find the killer

This game will make you a real detective, develop your logic

Implemented menu navigation

Different levels with questions

If the answer is correct, a panel appears with an expanded description of the answer

When pressing the wrong button, an error sound is played

There is a start over button

Added banner and interstitial advertising (google admob)


![alt text](https://gitlab.com/cartban1987/feb-2020.-find-the-killer/-/raw/main/menu.PNG)

![alt text](https://gitlab.com/cartban1987/feb-2020.-find-the-killer/-/raw/main/lvl1.PNG)

![alt text](https://gitlab.com/cartban1987/feb-2020.-find-the-killer/-/raw/main/lvl3.PNG)
